import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FizzBuzzTest {
	private static final int A_MULTIPLE_OF_3 = 3;
	private static final int A_MULTIPLE_OF_5 = 25;
	private static final int A_RANDOM_NUMBER = 4;
	private static final int A_RANDOM_NOT_RANGE_NUMBER=101;
	public FizzBuzz fizzbuzz;
	@Before
	public void init() {
		fizzbuzz = new FizzBuzz();
	}
	
	@Test
	public void WhenMultiplyingBy3_ShouldPrintFizz() {
		String result = fizzbuzz.printFizz(A_MULTIPLE_OF_3);
		assertEquals("Fizz",result);
		
	}
	
	@Test
	public void WhenMultiplyingByAMultipleOf5_ShouldReturnBuzz() {
		String result = fizzbuzz.printFizz(A_MULTIPLE_OF_5);
		assertEquals("Buzz",result);
	}
	
	@Test
	public void WhenMultiplyingByAnyOtherNumber_ShouldReturnFizzBuzz() {
		String result = fizzbuzz.printFizz(A_RANDOM_NUMBER);
		assertEquals("FizzBuzz",result);
	}
	
	@Test
	public void WhenMultiplyingByANotInRangeNumber_ShouldReturnErreur() {
		String result = fizzbuzz.printFizz(A_RANDOM_NOT_RANGE_NUMBER);
		assertEquals("Erreur",result);
	}

}
